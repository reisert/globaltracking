function [nii userdata] = DPS_mrs_to_nifti(mrs, filename,varargin)

% default data type is single
type = 'single';

switchworld = true;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end

if isstr(mrs),
    mrs = mrstruct_read(mrs);
end;

if isempty(mrs.edges)
    mrs.edges = eye(4);
end
if isempty(mrs.vox)
    mrs.vox = [1 1 1 1];
end

userdata = mrs.user;

mat = mrs.edges;
if switchworld,
    mat = diag([-1 -1 1 1])*mat;
end;

[qv T] = affine_to_quaternion(mat,mrs.vox(1:3));

if strcmp(type,'rgb'),
    % scale to 0...255 if rang 0..1 was given as input
    if max(mrs.dataAy(:)) <=1
        mrs.dataAy = mrs.dataAy*255;
    end
    
    mrs.dataAy = cast(mrs.dataAy,'uint8');
else
    mrs.dataAy = cast(mrs.dataAy,type);
end


nii = make_nii(mrs.dataAy,mrs.vox(1:3));

% set the array sizes
if strcmp(type,'rgb'),
    nii.hdr.dime.dim(1) = 3 + (size(mrs.dataAy, 4) > 3)  ;
    nii.hdr.dime.dim(5) = round( size(mrs.dataAy, 4) / 3);
else
    nii.hdr.dime.dim(1) = 3 + (size(mrs.dataAy, 4) > 1)  ;
    nii.hdr.dime.dim(5) = size(mrs.dataAy, 4);
end


if isfield(mrs.user, 'clim') & ~strcmp(type,'rgb'),
    nii.hdr.dime.cal_min = mrs.user.clim(1);
    nii.hdr.dime.cal_max = mrs.user.clim(2);
end


% intent_code used for colormap
% 0: gray
% 1: jet
if isfield(mrs.user, 'cmap') 
    %nii.hdr.dime.intent_code  = mrs.user.cmap;
    nii.hdr.hist.descrip  = sprintf('cmap:%i', mrs.user.cmap);
end

if strcmp(type,'single'),
    nii.hdr.dime.datatype = 16;
    nii.hdr.dime.bitpix = 32;
elseif strcmp(type,'uint8'),
    nii.hdr.dime.datatype = 2;
    nii.hdr.dime.bitpix = 8;
elseif strcmp(type,'int8'),
    nii.hdr.dime.datatype = 256;
    nii.hdr.dime.bitpix = 8;
elseif strcmp(type,'uint16'),
    nii.hdr.dime.datatype = 512;
    nii.hdr.dime.bitpix = 16;    
elseif strcmp(type,'int16'),
    nii.hdr.dime.datatype = 4;
    nii.hdr.dime.bitpix = 16;    
elseif strcmp(type,'rgb'),
    nii.hdr.dime.datatype = 128;
    nii.hdr.dime.bitpix = 24;    
end;


nii.hdr.dime.pixdim(1) = sign(det(mat));
%nii.hdr.dime.vox_offset = 352;
%nii.hdr.dime = nii_h.hdr.dime;

nii.hdr.hist.srow_x = mat(1,:);
nii.hdr.hist.srow_y = mat(2,:);
nii.hdr.hist.srow_z = mat(3,:);
nii.hdr.hist.qform_code = 1;
nii.hdr.hist.sform_code = 1;
nii.hdr.hist.qoffset_x = T(1);
nii.hdr.hist.qoffset_y = T(2);
nii.hdr.hist.qoffset_z = T(3);
nii.hdr.hist.quatern_b = -qv(2) * sign(qv(1)-eps);
nii.hdr.hist.quatern_c = -qv(3) * sign(qv(1)-eps);
nii.hdr.hist.quatern_d = -qv(4) * sign(qv(1)-eps);
nii.hdr.hist.magic = 'n+1';
nii.untouch = 1;
nii.edges = mat;
nii.voxsz = mrs.vox;
%nii.hdr.hist.aux_file = niit.hdr.hist.aux_file;


%% overwrite header if set as varargin input
if exist('hdr', 'var')
    fn = fieldnames(hdr);
    for h=1:length(fn)
        ff = fn{h};
        if isfield(nii.hdr.hk, ff)
            nii.hdr.hk.(ff) = hdr.(ff);
        elseif isfield(nii.hdr.dime, ff)
            nii.hdr.hk.(ff) = hdr.(ff);
        elseif isfield(nii.hdr.hist, ff)
            nii.hdr.hk.(ff) = hdr.(ff);
        end
    end
end

%%

% write nifti if requested as varargin
if nargin > 1, 
    if not(isempty(filename)),
        save_untouch_nii(nii, filename);
    end
end




function [qv T] = affine_to_quaternion(mat,vox)

R = inv(mat(1:3,1:3)*diag(1./vox));
 
 
R =  diag([1 1 sign(det(R))]) * R ;


T = mat(1:3,4);



Rxx = R(1,1); Rxy = R(1,2); Rxz = R(1,3);
Ryx = R(2,1); Ryy = R(2,2); Ryz = R(2,3);
Rzx = R(3,1); Rzy = R(3,2); Rzz = R(3,3);

w = sqrt( trace( R ) + 1 ) / 2;

% check if w is real. Otherwise, zero it.
if( imag( w ) > 0 )
     w = 0;
end

x = sqrt( 1 + Rxx - Ryy - Rzz ) / 2;
y = sqrt( 1 + Ryy - Rxx - Rzz ) / 2;
z = sqrt( 1 + Rzz - Ryy - Rxx ) / 2;

[element, i ] = max( [w,x,y,z] );

if( i == 1 )
    x = ( Rzy - Ryz ) / (4*w);
    y = ( Rxz - Rzx ) / (4*w);
    z = ( Ryx - Rxy ) / (4*w);
end

if( i == 2 )
    w = ( Rzy - Ryz ) / (4*x);
    y = ( Rxy + Ryx ) / (4*x);
    z = ( Rzx + Rxz ) / (4*x);
end

if( i == 3 )
    w = ( Rxz - Rzx ) / (4*y);
    x = ( Rxy + Ryx ) / (4*y);
    z = ( Ryz + Rzy ) / (4*y);
end

if( i == 4 )
    w = ( Ryx - Rxy ) / (4*z);
    x = ( Rzx + Rxz ) / (4*z);
    y = ( Ryz + Rzy ) / (4*z);
end

qv = [ w; x; y; z ];


% 
% 
% 
% 
% function [qv T] = affine_to_quaternion_old(mat,vox)
% 
% R = inv(mat(1:3,1:3)*diag(1./vox));
%  
% R = R * sign(det(R));
% 
% 
% r11 = R(1,1);
% r21 = R(2,1);
% r31 = R(3,1);
% r12 = R(1,2);
% r22 = R(2,2);
% r32 = R(3,2);
% r13 = R(1,3);
% r23 = R(2,3);
% r33 = R(3,3);
% 
% 
% q0 = ( r11 + r22 + r33 + 1.0) / 4.0;
% q1 = ( r11 - r22 - r33 + 1.0) / 4.0;
% q2 = (-r11 + r22 - r33 + 1.0) / 4.0;
% q3 = (-r11 - r22 + r33 + 1.0) / 4.0;
% if q0 < 0.0, q0 = 0.0; end;
% if q1 < 0.0, q1 = 0.0; end;
% if q2 < 0.0, q2 = 0.0; end;
% if q3 < 0.0, q3 = 0.0; end;
% q0 = sqrt(q0);
% q1 = sqrt(q1);
% q2 = sqrt(q2);
% q3 = sqrt(q3);
% SIGN = @(x) sign(x);
% NORM = @(x,y,z,d) sqrt(x^2+y^2+z^2+d^2);
% 
% 
% if q0 >= q1 && q0 >= q2 && q0 >= q3    
%     q1 = q1 * SIGN(r32 - r23);
%     q2 = q2 * SIGN(r13 - r31);
%     q3 = q3 * SIGN(r21 - r12);
% elseif q1 >= q0 && q1 >= q2 && q1 >= q3
%     q0 = q0 * SIGN(r32 - r23) ; %* (-1) ; 
%     q1 = q1                   ; %* (-1) ;
%     q2 = q2 * SIGN(r21 + r12)  ;
%     q3 = q3 * SIGN(r13 + r31)  ;
% elseif q2 >= q0 && q2 >= q1 && q2 >= q3
%     q0 = q0 * SIGN(r13 - r31);
%     q1 = q1 * SIGN(r21 + r12);
%     q3 = q3 * SIGN(r32 + r23);
% elseif q3 >= q0 && q3 >= q1 && q3 >= q2
%     q0 = q0 * SIGN(r21 - r12);
%     q1 = q1 * SIGN(r31 + r13) * (-1);
%     q2 = q2 * SIGN(r32 + r23) * (-1);    
%  else 
%     error('problem');
%  end;
% r = NORM(q0, q1, q2, q3);
% q0 = q0*r;
% q1 = q1*r;
% q2 = q2*r;
% q3 = q3*r;
% qv = [q0 q1 q2 q3];
% 
% T = mat(1:3,4);

% 
% 
% %%
% 
% hdr = niit.hdr;
% 
% %% qform to affine
% 
%       b = hdr.hist.quatern_b;
%       c = hdr.hist.quatern_c;
%       d = hdr.hist.quatern_d;
% 
%       i = hdr.dime.pixdim(2);
%       j = hdr.dime.pixdim(3);
%       k = qfac * hdr.dime.pixdim(4);
%       
%       if 1.0-(b*b+c*c+d*d) < 0
%          if abs(1.0-(b*b+c*c+d*d)) < 1e-5
%             a = 0;
%          else
%             error('Incorrect quaternion values in this NIFTI data.');
%          end
%       else
%          a = sqrt(1.0-(b*b+c*c+d*d));
%       end
% 
%       qfac = hdr.dime.pixdim(1);
%       if qfac==0, qfac = 1; end
%       i = hdr.dime.pixdim(2);
%       j = hdr.dime.pixdim(3);
%       k = qfac * hdr.dime.pixdim(4);
% 
%       R = [a*a+b*b-c*c-d*d     2*b*c-2*a*d        2*b*d+2*a*c
%            2*b*c+2*a*d         a*a+c*c-b*b-d*d    2*c*d-2*a*b
%            2*b*d-2*a*c         2*c*d+2*a*b        a*a+d*d-c*c-b*b];
%        
%       R = R * diag([i j k]);
%       
%       T = [hdr.hist.qoffset_x
%            hdr.hist.qoffset_y
%            hdr.hist.qoffset_z];
% 
% R
% 
% %%
% 
% 
%    R = [hdr.hist.srow_x(1:3)
%            hdr.hist.srow_y(1:3)
%            hdr.hist.srow_z(1:3)]
% 
%       T = [hdr.hist.srow_x(4)
%            hdr.hist.srow_y(4)
%            hdr.hist.srow_z(4)];
% 
% 
% 
% 
% 
% 
% 
% 
