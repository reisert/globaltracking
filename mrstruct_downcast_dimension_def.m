%MRSTRUCT_DOWNCAST_DIMENSION_DEF default dimension for mrStruct downcast.
%		removeDim = mrstruct_downcast_dimension_def(sourceTypeStr,targetTypeStr)
%
%		'removeDim': integer of the dimension which is squeezed per default. Is 0 on error.
%		'sourceTypeStr': type name of mrStruct to be converted.
%		'targetTypeStr': type name in which mrStuct shall be converted.
%
%		Examples:
%		removeDim = mrstruct_downcast_dimension_def('volume','image');																	
%										-> removeDim = 3;
%
%		Harald Fischer
%		5/00
%
%		PC




function removeDim = mrstruct_downcast_dimension_def(sourceTypeStr,targetTypeStr)



%%%%% init and error check
removeDim = 0;

if nargin==2
   if ~ischar(sourceTypeStr) | ~ischar(targetTypeStr)
      warning('input arguments have wrong type');
      return;
   end
else 
   warning('insufficient input arguments');
   return;
end

if mrstruct_iscast(sourceTypeStr,targetTypeStr)~=2
   warning('downcast from sourceType to targetType not possible');
   return;
end
%%%%% End of: init and error check




%%%%% main
if strcmp(sourceTypeStr,'image')
   removeDim = 1;
   %'spectrum';   
   
elseif strcmp(sourceTypeStr,'imageEchos')
   if strcmp(targetTypeStr,'image')
      removeDim = 3;
   else
      removeDim = 1;
   end
   %'image' 'spectrum1D';   
   
elseif strcmp(sourceTypeStr,'volume')
   if strcmp(targetTypeStr,'image')
      removeDim = 3;
   else
      removeDim = 1;
   end
   %'image' 'spectrum1D';   
   
elseif strcmp(sourceTypeStr,'volumeEchos')
   if strcmp(targetTypeStr,'imageEchos') | strcmp(targetTypeStr,'series2D')
      removeDim = 3;
   elseif strcmp(targetTypeStr,'sprectrum2D')
      removeDim = 1;
   else
      removeDim = 4;
   end
   %'volume' 'imageEchos' 'series2D' 'spectrum2D';   
   
elseif strcmp(sourceTypeStr,'series2D')
   if strcmp(targetTypeStr,'image')
      removeDim = 3;
   else
      removeDim = 1;
   end
   %'image' 'spectrum1D';   
   
elseif strcmp(sourceTypeStr,'series2DEchos')
   if strcmp(targetTypeStr,'series2D')
      removeDim = 3;
   elseif strcmp(targetTypeStr,'sprectrum2D')
      removeDim = 1;
   else
      removeDim = 4;
   end
   %'volume' 'imageEchos' 'series2D' 'spectrum2D';   
      
elseif strcmp(sourceTypeStr,'series3D')
   if strcmp(targetTypeStr,'volume') | strcmp(targetTypeStr,'imageEchos')
      removeDim = 4;
   elseif strcmp(targetTypeStr,'series2D')
      removeDim = 3;
   else
      removeDim = 1;
   end
   %'volume' 'imageEchos' 'series2D' 'spectrum2D';   
   
elseif strcmp(sourceTypeStr,'series3DEchos')
   if strcmp(targetTypeStr,'volumeEchos')
      removeDim = 5;
   elseif strcmp(targetTypeStr,'series2DEchos')
      removeDim = 3;
   elseif strcmp(targetTypeStr,'series3D')
      removeDim = 4;
   else
      removeDim = 1;
   end
   %'volumeEchos' 'series2DEchos' 'series3D' 'spectrum3D';   
   
elseif strcmp(sourceTypeStr,'spectrum')
   removeDim = 0;
   %'';   
   
elseif strcmp(sourceTypeStr,'spectrum1D')
   removeDim = 1;
   %'spectrum';   
   
elseif strcmp(sourceTypeStr,'spectrum2D')
   if strcmp(targetTypeStr,'image')
      removeDim = 3;
   else
      removeDim = 2;
   end
   %'image' 'spectrum1D';   
   
elseif strcmp(sourceTypeStr,'spectrum3D')
   if strcmp(targetTypeStr,'spectrum2D')
      removeDim = 3;
   else
      removeDim = 4;
   end
   %'volume' 'imageEchos' 'series2D' 'spectrum2D';   
   
end
%%%%% End of: main



% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
