%MRSTRUCT_ISCAST 
%		Harald Fischer
%		12/99
%
%		PC
% returns -1 on error, 0 for alternative, 1 for upcast and 2 for downcast



function castFlag = mrstruct_iscast(sourceTypeStr,targetTypeStr)



%%%%% init and error check
if nargin==2
   if ~ischar(sourceTypeStr) | ~ischar(targetTypeStr)
      warning('input argument has wrong type');
      return;
   end
else 
   warning('insufficient input arguments');
   return;
end

altStrMx = {};
castFlag = -1;
%%%%% End of: init and error check



%%%%% main
[hits,altStrMx] = mrstruct_alternatives(sourceTypeStr,0);
for index=1:hits
   if strcmp(altStrMx(index),targetTypeStr)
      castFlag = 0;
      return;
   end
end


[hits,altStrMx] = mrstruct_alternatives(sourceTypeStr,1);
for index=1:hits
   if strcmp(altStrMx(index),targetTypeStr)
      castFlag = 1;
      return;
   end
end


[hits,altStrMx] = mrstruct_alternatives(sourceTypeStr,2);
for index=1:hits
   if strcmp(altStrMx(index),targetTypeStr)
      castFlag = 2;
      return;
   end
end
%%%%%  End of: main





% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
