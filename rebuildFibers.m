% ftr = rebuildFibers(ftr,fiblen)
%
% builds fibers from internal Gibbstracking state, necessary if different 
% fiberlength restrictions are desired
%
% example usage:
%
% newftr = rebuildFiber('my_FTR.mat',[4;inf]);
% or 
% ftr = ftrstruct_read('my_FTR.mat');
% newftr = rebuildFiber(ftr,[4;inf]);
% ftrstruct_write(newftr,'other_FTR.mat');

function ftr = rebuildFibers(ftr,fiblen)

if isstr(ftr)
    ftr = ftrstruct_read(ftr)
end


fibidx = BuildFibres(ftr.user,fiblen);
offs = diag([ftr.vox 0]);
info.name = 'newpatient';
info.edges = ftr.hMatrix;
newftr = saveFTR('newname',ftr.user,fibidx,offs,info,false);
ftr.connectCell = newftr.connectCell;
ftr.curveSegCell = newftr.curveSegCell;
