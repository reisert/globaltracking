function smODF = smoothHardi(ODF,bDir,numit)

iso = 1;
smODF = ODF;
for k = 1:size(bDir,2);
    D = eye(3)/3*iso + single(bDir(:,k)*bDir(:,k)')*(1-iso); 
    smODF(:,:,:,k) = anisoDiffusionHomogenous(single(squeeze(ODF(:,:,:,k))),[D(1,1) D(2,2) D(3,3) D(1,2) D(1,3) D(2,3)]',single([0.1 numit]));
    fprintf('.');
end;