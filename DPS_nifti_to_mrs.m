function mrs = DPS_nifti_to_mrs(nii)

if isstr(nii),
    nii = load_untouch_nii(nii);
end;

nii.mat = [nii.hdr.hist.srow_x ; nii.hdr.hist.srow_y ; nii.hdr.hist.srow_z; 0 0 0 1];
if ndims(nii.img) > 3
    
    %mrs = mrstruct_init( 'series3D', double(nii.img) );
    mrs = mrstruct_init( 'series3D', single(nii.img) );
else
    %mrs = mrstruct_init( 'volume', double(nii.img) );
    mrs = mrstruct_init( 'volume', single(nii.img) );
end

mrs.vox = sqrt( sum((nii.mat*diag([1 1 1 0])).^2) );
mrs.edges = nii.mat;
mrs.edges = diag([-1 -1 1 1 ]) * mrs.edges;

%% consider the slope
if(~isnan(nii.hdr.dime.scl_slope) & nii.hdr.dime.scl_slope ~= 0)
    mrs.dataAy = mrs.dataAy * nii.hdr.dime.scl_slope + nii.hdr.dime.scl_inter;
end


%% extract TR and TE
% first, calc by slice_duration, then check descrip (and other) fields
% (backwards compatibilty
if nii.hdr.dime.slice_duration > 0
   mrs.tr = nii.hdr.dime.slice_duration * nii.hdr.dime.dim(4);
end

if ~isempty(nii.hdr.hk.db_name)
    
    tString = nii.hdr.hk.db_name;
    %names = regexp(tString, '?TR:(?<TR>.+)\sTE:(?<TE>.+)', 'names');
    
    TR = regexp(tString, 'TR:\d+', 'match');
    if ~isempty(TR)
        TR  = TR{1}; 
        mrs.tr = str2double(TR(4:end));
    end
    
    TE = regexp(tString, 'TE:\d+', 'match');
    if ~isempty(TE)
        TE  = TE{1}; 
        mrs.te = str2double(TE(4:end));
    end
end

% load also the clims? 
% no, do not do this for now, if we create another mrstruct based on this will have
% same clims ...
% mrs.user.clim = [nii.hdr.dime.cal_min, nii.hdr.dime.cal_max];
