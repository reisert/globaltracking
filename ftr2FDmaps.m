function [fibdensRGB fibdens epdens] = ftr2FDmaps(ftr,sz,sel,M)


if not(isempty(sel)),
    if isfield(ftr,'fiber'),
        idx = ftr.fiber{sel}.curveID;
    else
        idx = sel;
    end;
else    
    idx = 1:length(ftr.curveSegCell);
end;
idx = idx(cellfun(@length,ftr.curveSegCell(idx))>1);
vox = ftr.vox;


thefibs = ftr.curveSegCell(idx);
%sk = 50;
%thefibs = cellfun(@(x) x(min(end,sk):max(1,end-sk+1),:),thefibs,'UniformOutput',false);

nc = cellfun(@(x) ip(x,ceil(M*3)),thefibs,'UniformOutput',false);
nd = cellfun(@(x) id(x,ceil(M*3)),thefibs,'UniformOutput',false);


if length(nc) == 0,
    fibdens = zeros(single(floor(sz*M)));
    epdens = zeros(single(floor(sz*M)));
    fibdensRGB = zeros([single(floor(sz*M)) 3]);
    return;
end


for k = 1:length(nc),
    sg = 1;
    if isfield(ftr,'signs')
        sg = ftr.signs(k);
    end
    dir = nd{k};
    dir(:,1) = dir(:,1)*sg;
    dir(:,2) = dir(:,2)*sg;
    dir(:,3) = dir(:,3)*sg;
    arclen = sqrt(sum(dir.^2,2));
    ncs{k} = dir;
    ncal{k} = arclen;
end;

pts  = cat(1,nc{:})-1;
dir = cat(1,ncs{:});
arclen = cat(1,ncal{:});
% perm = [2 1 3];
% for k = 1:3,
%     fibdensRGB(:,:,:,perm(k)) = AccumulateBilinWeighted(single(floor(sz*M)),single(pts'*M),single(abs(dir(:,k))));
% end;

%dir = dir(:,[2 1 3]);
fibdensRGB = AccumulateVectorF(single(floor(sz*M)),single(pts'*M)+0.5,(single(dir)')); 

fprintf('.');
fibdens = AccumulateBilinWeighted(single(floor(sz*M)),single(pts'*M),single(arclen)); 


nc = cellfun(@ep,ftr.curveSegCell(idx),'UniformOutput',false);
pts  = cat(1,nc{:})-1;
epdens = AccumulateBilin(single(floor(sz*M)),single(pts'*M)); 



return;


function y = ip(x,N)
dx = x(2:end,:) - x(1:end-1,:);
x = x(1:end-1,:);
y = ones(N,size(x,1),size(x,2));
for k = 1:N,
    y(k,:,:) = x + k/N*dx ;
end;
y = reshape(y,[N*size(x,1) size(x,2)]);

function y = id(x,N)
dx = x(2:end,:) - x(1:end-1,:);
x = x(1:end-1,:);
y = ones(N,size(x,1),size(x,2));
for k = 1:N,
    y(k,:,:) = dx ;
end;
y = reshape(y,[N*size(x,1) size(x,2)]);



function y = ep(x)
y = [x(1,:) ; x(end,:)];
