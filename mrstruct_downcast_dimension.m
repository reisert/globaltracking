%MRSTRUCT_DOWNCAST_DIMENSION possible dimensions for mrStruct downcast.
%		[removeDimStrMx,removeDimVc] = mrstruct_downcast_dimension(sourceTypeStr,targetTypeStr)
%
%		'removeDimStrMx': Cell array of possible dimension to be squeezed ({'size_z','size_t'},...).
%		'removeDimVc': 9x1 Vector of possible dimensions to be squeezed.
%						([0;0;1;0;...]) indicates that mrStruct.dim3 can be squeezed.
%
%		Examples:
%		[removeDimStrMx,removeDimVc] = mrstruct_downcast_dimension('volume','image');																	
%										-> removeDimStrMx= {'size_z'}
%										-> removeDimVc = [0;0;1;0;...;0];
%
%		Harald Fischer
%		1/00
%
%		PC




function [removeDimStrMx,removeDimVc] = mrstruct_downcast_dimension(sourceTypeStr,targetTypeStr)



%%%%% init and error check
if nargin==2
   if ~ischar(sourceTypeStr) | ~ischar(targetTypeStr)
      warning('input arguments have wrong type');
      return;
   end
else 
   warning('insufficient input arguments');
   return;
end

removeDimStrMx = {};
removeDimVc    = [];
%%%%% End of: init and error check




%%%%% main
if strcmp(sourceTypeStr,'image')
   if strcmp(targetTypeStr,'spectrum')
      removeDimVc    = [2];
      removeDimStrMx = {'size_y'};
   end
   
elseif strcmp(sourceTypeStr,'imageEchos')
   if strcmp(targetTypeStr,'image')
      removeDimVc    = [3];
      removeDimStrMx = {'echos'};
   elseif strcmp(targetTypeStr,'spectrum1D')
      removeDimVc    = [1;2;3];
      removeDimStrMx = {'size_x';'size_y';'echos'};
   end
  
elseif strcmp(sourceTypeStr,'volume')
   if strcmp(targetTypeStr,'image')
      removeDimVc    = [3];
      removeDimStrMx = {'size_z'};
   elseif strcmp(targetTypeStr,'spectrum1D')
      removeDimVc    = [1;2;3];
      removeDimStrMx = {'size_x';'size_y';'size_z'};
   end
   
elseif strcmp(sourceTypeStr,'volumeEchos')
   if strcmp(targetTypeStr,'volume')
      removeDimVc    = [4];
      removeDimStrMx = {'echos'};
   elseif strcmp(targetTypeStr,'imageEchos')
      removeDimVc    = [3];
      removeDimStrMx = {'size_z'};
   elseif strcmp(targetTypeStr,'series2D')
      removeDimVc    = [3;4];
      removeDimStrMx = {'size_z';'echos'};
   elseif strcmp(targetTypeStr,'spectrum2D')
      removeDimVc    = [1;2;3;4];
      removeDimStrMx = {'size_x';'size_y';'size_z';'echos'};
   end
   
elseif strcmp(sourceTypeStr,'series2D')
   if strcmp(targetTypeStr,'image')
      removeDimVc    = [3];
      removeDimStrMx = {'size_t'};
   elseif strcmp(targetTypeStr,'spectrum1D')
      removeDimVc    = [1;2;3];
      removeDimStrMx = {'size_x';'size_y';'size_t'};
   end

elseif strcmp(sourceTypeStr,'series2DEchos')
   if strcmp(targetTypeStr,'volume')
      removeDimVc    = [3;4];
      removeDimStrMx = {'echos';'size_t'};
   elseif strcmp(targetTypeStr,'imageEchos')
      removeDimVc    = [4];
      removeDimStrMx = {'size_t'};
   elseif strcmp(targetTypeStr,'series2D')
      removeDimVc    = [3];
      removeDimStrMx = {'echos'};
   elseif strcmp(targetTypeStr,'spectrum2D')
      removeDimVc    = [1;2;3;4];
      removeDimStrMx = {'size_x';'size_y';'echos';'size_t'};
   end
      
elseif strcmp(sourceTypeStr,'series3D')
   if strcmp(targetTypeStr,'volume')
      removeDimVc    = [4];
      removeDimStrMx = {'size_t'};
   elseif strcmp(targetTypeStr,'imageEchos')
      removeDimVc    = [3;4];
      removeDimStrMx = {'size_z';'size_t'};
   elseif strcmp(targetTypeStr,'series2D')
      removeDimVc    = [3];
      removeDimStrMx = {'size_z'};
   elseif strcmp(targetTypeStr,'spectrum2D')
      removeDimVc    = [1;2;3;4];
      removeDimStrMx = {'size_x';'size_y';'size_z';'size_t'};
   end
   
elseif strcmp(sourceTypeStr,'series3DEchos')
   if strcmp(targetTypeStr,'volumeEchos')
      removeDimVc    = [5];
      removeDimStrMx = {'size_t'};
   elseif strcmp(targetTypeStr,'series2DEchos')
      removeDimVc    = [3];
      removeDimStrMx = {'size_z'};
   elseif strcmp(targetTypeStr,'series3D')
      removeDimVc    = [4];
      removeDimStrMx = {'echos'};
   elseif strcmp(targetTypeStr,'spectrum3D')
      removeDimVc    = [1;2;3;4;5];
      removeDimStrMx = {'size_x';'size_y';'size_z';'echos';'size_t'};
   end
      
elseif strcmp(sourceTypeStr,'spectrum')
   % can not be downcasted any more   
   
elseif strcmp(sourceTypeStr,'spectrum1D')
   if strcmp(targetTypeStr,'spectrum')
      removeDimVc    = [1];
      removeDimStrMx = {'size_x'};
   end
   
elseif strcmp(sourceTypeStr,'spectrum2D')
   if strcmp(targetTypeStr,'image')
      removeDimVc    = [3];
      removeDimStrMx = {'spectral'};
   elseif strcmp(targetTypeStr,'spectrum1D')
      removeDimVc    = [1;2];
      removeDimStrMx = {'size_x';'size_y'};
   end
   
elseif strcmp(sourceTypeStr,'spectrum3D')
   if strcmp(targetTypeStr,'volume')
      removeDimVc    = [4];
      removeDimStrMx = {'spectral'};
   elseif strcmp(targetTypeStr,'imageEchos')
      removeDimVc    = [3;4];
      removeDimStrMx = {'size_z';'spectral'};
   elseif strcmp(targetTypeStr,'series2D')
      removeDimVc    = [3;4];
      removeDimStrMx = {'size_z';'spectral'};
   elseif strcmp(targetTypeStr,'spectrum2D')
      removeDimVc    = [3];
      removeDimStrMx = {'size_z'};
   end
   
end
%%%%% End of: main



% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
