
This repository contains the MATLAB implementation of the Global tracking algorithm as proposed in the paper

# Global fiber reconstruction becomes practical
M Reisert, I Mader, C Anastasopoulos, M Weigel, S Schnell, V Kiselev
Neuroimage 54 (2), 955-962

### How to set it up

* Clone the repository into some folder
* Set the MATLAB path to the contained folder (including subdirectories) by addpath(genpath('/path/to/globaltracking'))
* The mex-files are precompiled for matlab >= 2012b (there is a #define NEWMATLAB compile flag which has to be disabled to run on earlier versions)
* Depending on your machine you have to rebuild the C++ binaries, therefore mex has to be set up. Use ft_make.m to build everything needed.
* Write fiberGT_tool at the MATLAB console and start
* Have Fun!

