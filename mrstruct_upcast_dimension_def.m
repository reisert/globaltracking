%MRSTRUCT_UPCAST_DIMENSION_DEF default dimension for mrStruct downcast.
%		insertDim = mrstruct_upcast_dimension_def(sourceTypeStr,targetTypeStr)
%
%		'insertDim': integer of the new dimension. Is 0 on error.
%		'sourceTypeStr': type name of mrStruct to be converted.
%		'targetTypeStr': type name in which mrStuct shall be converted.
%
%		Examples:
%		insertDim = mrstruct_upcast_dimension_def('image','volume');																	
%										-> insertDim = 3;
%
%		Harald Fischer
%		5/00
%
%		PC




function insertDim = mrstruct_upcast_dimension_def(sourceTypeStr,targetTypeStr)



%%%%% init and error check
insertDim = 0;

if nargin==2
   if ~ischar(sourceTypeStr) | ~ischar(targetTypeStr)
      warning('input arguments have wrong type');
      return;
   end
else 
   warning('insufficient input arguments');
   return;
end

if mrstruct_iscast(sourceTypeStr,targetTypeStr)~=1
   warning('upcast from sourceType to targetType not possible');
   return;
end
%%%%% End of: init and error check




%%%%% main
if strcmp(sourceTypeStr,'image')
   insertDim = 3;
   %'imageEchos' 'volume' 'series2D' 'spectrum2D'
     
elseif strcmp(sourceTypeStr,'imageEchos')
   if strcmp(targetTypeStr,'volumeEchos')
      insertDim = 3;  
   else
      insertDim = 4;
   end
   %'volumeEchos' 'series2DEchos' 'series3D' 'spectrum3D'   
   
elseif strcmp(sourceTypeStr,'volume')
   insertDim = 4;
   %'volumeEchos' 'series2DEchos' 'series3D' 'spectrum3D'   
   
elseif strcmp(sourceTypeStr,'volumeEchos')
   insertDim = 5;
   %'series3DEchos'   
   
elseif strcmp(sourceTypeStr,'series2D')
   if strcmp(targetTypeStr,'volumeEchos') | strcmp(targetTypeStr,'series3D') | strcmp(targetTypeStr,'series2DEchos')
      insertDim = 3;
   elseif strcmp(targetTypeStr,'spectrum3D')
      insertDim = 4;
   end
   %'volumeEchos' 'series2DEchos' 'series3D' 'spectrum3D'   
   
elseif strcmp(sourceTypeStr,'series2DEchos')
   insertDim = 3;
   %'series3DEchos'   
   
elseif strcmp(sourceTypeStr,'series3D')
   insertDim = 4;
   %'series3DEchos'   
   
elseif strcmp(sourceTypeStr,'series3DEchos')
   insertDim = 0;
   %''   
   
elseif strcmp(sourceTypeStr,'spectrum')
   insertDim = 1;
   %'image' 'spectrum1D'   
   
elseif strcmp(sourceTypeStr,'spectrum1D')
   if strcmp(targetTypeStr,'spectrum2D')
      insertDim = 2;
   else
      insertDim = 1;
   end
   %'imageEchos' 'volume' 'series2D' 'spectrum2D';
   
elseif strcmp(sourceTypeStr,'spectrum2D')
   if strcmp(targetTypeStr,'spectrum3D')
      insertDim = 3;
   else
      insertDim = 1;
   end
   %'volumeEchos' 'series2DEchos' 'series3D' 'spectrum3D';   
   
elseif strcmp(sourceTypeStr,'spectrum3D')
   insertDim = 1;
   %'series3DEchos';   
   
end
%%%%% End of: main



% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
