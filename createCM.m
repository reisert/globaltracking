

function mstruc = createConMatrix(ftrdata,roidata,Nsz)


fuzzysigma = Nsz; 

%%
display('loading')
roi_fi = DPX_selectFiles(roidata);

ROI = DPS_nifti_to_mrs(roi_fi{1});

ftr_fi = DPX_selectFiles(ftrdata);

ftr = ftrstruct_read(ftr_fi{1});

%%
display('transforming fibers to ROI slicing')
fibs = ftr.curveSegCell;
T = inv(ROI.edges)*ftr.hMatrix;
fibs = cellfun(@(x) (x) * T(1:3,1:3)' + repmat(T(1:3,4)',[size(x,1) 1]) ,fibs,'UniformOutput',false);

display('generating vertex array')
vertex =  cat(1,fibs{:});
sz = size(ROI.dataAy);
vertex(vertex(:,1)<1,1) = 1;
vertex(vertex(:,2)<1,2) = 1;
vertex(vertex(:,3)<1,3) = 1;
vertex(vertex(:,1)>sz(1),1) = sz(1);
vertex(vertex(:,2)>sz(2),2) = sz(2);
vertex(vertex(:,3)>sz(3),3) = sz(3);
lens = cellfun(@(x) size(x,1), fibs);
cumlens = cumsum(lens);




%%
display('traversing over rois')

rois = ROI.dataAy;
rois(isnan(rois(:))) = 0;
sigma = fuzzysigma/max(ROI.vox);

ridx = unique(rois(:));
Xmat = zeros(length(fibs),length(ridx)-1);
for k = 2:length(ridx),
    vertex = round(vertex);        
    idx = vertex(:,1) + sz(1)*(vertex(:,2)-1) + (sz(1)*sz(2))*(vertex(:,3)-1);
    roi = rois==ridx(k);
    if fuzzysigma > 0,
        roi = bwdist(roi);
        roi = exp(-roi.^2 /(2*sigma^2));
    end
    cmpall = roi(idx);      
    
    cumcmpall = cumsum(cmpall);
    overlap = (cumcmpall([cumlens]) - [0 ; cumcmpall(cumlens(1:end-1))]);

    Xmat(:,k-1) =  overlap;
    fprintf('.')
end
%%
Xmat(Xmat>1) = 1;

mstruc.cc =  (Xmat'*Xmat);      %% connec matrix
mstruc.lens =  (Xmat'*(Xmat.*repmat(lens,[1 size(Xmat,2)]))) ./ mstruc.cc;






