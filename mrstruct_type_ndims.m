%MRSTRUCT_TYPE_NDIMS return meaning of dimension in mrStruct. 
%		typeDim = mrstruct_type_ndims(typeStr)
%
%		'typeStr': the data type.
%		'typeDim' : Integer, number of dimensions.
%
%		Examples:
%		typeDim = mrstruct_type_ndims('volume'); 	-> typeDim = 3																
%
%		Harald Fischer
%		1/00
%
%		PC



function typeDim = mrstruct_type_ndims(typeStr)



%%%%% init and error check
if nargin==1
   if ~ischar(typeStr)
      warning('input argument has wrong type');
      return;
   end
else 
   warning('insufficient input arguments');
   return;
end
typeDim = 0;
%%%%% End of: init and error check




if strcmp(typeStr,'image')
   typeDim = 2;
   
elseif strcmp(typeStr,'imageEchos')
   typeDim = 3;
   
elseif strcmp(typeStr,'volume')
   typeDim = 3;
   
elseif strcmp(typeStr,'volumeEchos')
   typeDim = 4;
   
elseif strcmp(typeStr,'series2D')
   typeDim = 3;
   
elseif strcmp(typeStr,'series2DEchos')
   typeDim = 4;
      
elseif strcmp(typeStr,'series3D')
   typeDim = 4;
   
elseif strcmp(typeStr,'series3DEchos')
   typeDim = 5;
   
elseif strcmp(typeStr,'spectrum')
   typeDim = 1;
   
elseif strcmp(typeStr,'spectrum1D')
   typeDim = 2;
   
elseif strcmp(typeStr,'spectrum2D')
   typeDim = 3;
   
elseif strcmp(typeStr,'spectrum3D')
   typeDim = 4;
   
end



% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
