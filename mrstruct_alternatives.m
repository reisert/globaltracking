%MRSTRUCT_ALTERNATIVES possible types for mrStruct type conversion.
%		[hits,alternativesStrMx] = mrstruct_alternatives(typeStr,flag)
%
%		The type of a mrStruct can be modified when the dimensionality 
%		stays the same or changes by 1 ('image'->'volume', 'series3D'->'imageEchos',...).
%		'hits': number of alternative MR-Structure data types.
%		'alternativesStrMx': Cell array of strings indicating the possible data types.
%		'flag': flag=0 returns conversion without cast, flag=1 for upcast (increase
%					in dimensionality), flag=2 for downcast and flag=3 for all alternatives.
%					flag=0 is default and used when nargin=1.
%		typeStr = {image,imageEchos,volume,volumeEchos,series2D,series2DEchos,
%					spectrum,spectrum1D,spectrum2D,spectrum3D} (required).
%
%		Examples:
%		[hits, altStrMx] = mrstruct_alternatives('volume',2);		-> hits=0
%																			-> altStrMx = {'image';'spectrum1D'}
%
%		Harald Fischer
%		1/00
%
%		PC



function [hits,alternativesStrMx] = mrstruct_alternatives(typeStr,flag)



%%%%% init and error check
if nargin==1 | nargin==2
   if ~ischar(typeStr)
      warning('input argument has wrong type');
      return;
   end
   if nargin==2
      if ~isnumeric(flag)
         warning('input argument has wrong type');
         return;
      end
   else
      flag = 0;
   end
else 
   warning('insufficient input arguments');
   return;
end

alternativesStrMx = {};                               %%% Achtung, auf cell ay ge�ndert
hits              = 0;
%%%%% End of: init and error check



%%%%% main

%%% lookup alternatives without cast
if flag==0
   [hits, alternativesStrMx] = local_alternatives(typeStr);
%%% End of: lookup alternatives without cast


%%% upcast
elseif flag==1
   [hits, alternativesStrMx] = local_alternatives_upcast(typeStr);
%%% End of: upcast


%%% downcast
elseif flag==2
   [hits, alternativesStrMx] = local_alternatives_downcast(typeStr);
%%% End of: downcast


%%% all
elseif flag==3
   [hits, alternativesStrMx] = local_alternatives_all(typeStr);
%%% End of: all


else
   warning('input value not recognized');
end
%%%%% End of: main






%%%%%%%%%%%%%%%%%%%%% local functions %%%%%%%%%%%%%%%%%%%%%%%%



function [num, alternativesStrMx] = local_alternatives_downcast(typeStr)

num     = 0;
alt1Str = '';
alt2Str = '';
alt3Str = '';
alt4Str = '';
alternativesStrMx = {};

if strcmp(typeStr,'image')
   alt1Str = 'spectrum';   
   
elseif strcmp(typeStr,'imageEchos')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'volume')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'volumeEchos')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';   
   
elseif strcmp(typeStr,'series2D')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'series2DEchos')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';   
      
elseif strcmp(typeStr,'series3D')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';   
   
elseif strcmp(typeStr,'series3DEchos')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'spectrum')
   alt1Str = '';   
   
elseif strcmp(typeStr,'spectrum1D')
   alt1Str = 'spectrum';   
   
elseif strcmp(typeStr,'spectrum2D')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'spectrum3D')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';   
   
end

if ~isempty(alt1Str) num = num+1; end
if ~isempty(alt2Str) num = num+1; end
if ~isempty(alt3Str) num = num+1; end
if ~isempty(alt4Str) num = num+1; end

if num==1
   alternativesStrMx = {alt1Str};
elseif num==2
   alternativesStrMx = {alt1Str;alt2Str};
elseif num==3
   alternativesStrMx = {alt1Str;alt2Str;alt3Str};
elseif num==4
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str};
end




function [num, alternativesStrMx] = local_alternatives_upcast(typeStr)

num     = 0;
alt1Str = '';
alt2Str = '';
alt3Str = '';
alt4Str = '';
alternativesStrMx = {};

if strcmp(typeStr,'image')
   alt1Str = 'imageEchos';   
   alt2Str = 'volume';
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';
   
elseif strcmp(typeStr,'imageEchos')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'volume')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'volumeEchos')
   alt1Str = 'series3DEchos';   
   
elseif strcmp(typeStr,'series2D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'series2DEchos')
   alt1Str = 'series3DEchos';   
      
elseif strcmp(typeStr,'series3D')
   alt1Str = 'series3DEchos';   
   
elseif strcmp(typeStr,'series3DEchos')
   alt1Str = '';   
   
elseif strcmp(typeStr,'spectrum')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'spectrum1D')
   alt1Str = 'imageEchos';   
   alt2Str = 'volume';
   alt3Str = 'series2D';   
   alt4Str = 'spectrum2D';
   
elseif strcmp(typeStr,'spectrum2D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'spectrum3D')
   alt1Str = 'series3DEchos';   
   
end

if ~isempty(alt1Str) num = num+1; end
if ~isempty(alt2Str) num = num+1; end
if ~isempty(alt3Str) num = num+1; end
if ~isempty(alt4Str) num = num+1; end

if num==1
   alternativesStrMx = {alt1Str};
elseif num==2
   alternativesStrMx = {alt1Str;alt2Str};
elseif num==3
   alternativesStrMx = {alt1Str;alt2Str;alt3Str};
elseif num==4
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str};
end



function [num, alternativesStrMx] = local_alternatives(typeStr)

num     = 0;
alt1Str = '';
alt2Str = '';
alt3Str = '';
alt4Str = '';
alternativesStrMx = {};

if strcmp(typeStr,'image')
   alt1Str = 'spectrum1D';   
   
elseif strcmp(typeStr,'imageEchos')
   alt1Str = 'volume';   
   alt2Str = 'series2D';   
   alt3Str = 'spectrum2D';   
   
elseif strcmp(typeStr,'volume')
   alt1Str = 'imageEchos';   
   alt2Str = 'series2D';   
   alt3Str = 'spectrum2D';   
   
elseif strcmp(typeStr,'volumeEchos')
   alt1Str = 'series2DEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'series2D')
   alt1Str = 'imageEchos';   
   alt2Str = 'volume';   
   alt3Str = 'spectrum2D';   
   
elseif strcmp(typeStr,'series2DEchos')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';   
      
elseif strcmp(typeStr,'series3D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';   
   
elseif strcmp(typeStr,'series3DEchos')
   alt1Str = '';   
   
elseif strcmp(typeStr,'spectrum')
   alt1Str = '';   
   
elseif strcmp(typeStr,'spectrum1D')
   alt1Str = 'image';   
   
elseif strcmp(typeStr,'spectrum2D')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';   
   
elseif strcmp(typeStr,'spectrum3D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   
end

if ~isempty(alt1Str) num = num+1; end
if ~isempty(alt2Str) num = num+1; end
if ~isempty(alt3Str) num = num+1; end
if ~isempty(alt4Str) num = num+1; end

if num==1
   alternativesStrMx = {alt1Str};
elseif num==2
   alternativesStrMx = {alt1Str;alt2Str};
elseif num==3
   alternativesStrMx = {alt1Str;alt2Str;alt3Str};
elseif num==4
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str};
end





function [num, alternativesStrMx] = local_alternatives_all(typeStr)

num     = 0;
alt1Str = '';
alt2Str = '';
alt3Str = '';
alt4Str = '';
alt5Str = '';
alt6Str = '';
alt7Str = '';
alt8Str = '';
alt9Str = '';

alternativesStrMx = {};

if strcmp(typeStr,'image')
   alt1Str = 'spectrum1D';
   alt2Str = 'spectrum';
   alt3Str = 'imageEchos';   
   alt4Str = 'volume';
   alt5Str = 'series2D';   
   alt6Str = 'spectrum2D';
   
elseif strcmp(typeStr,'imageEchos')
   alt1Str = 'volume';   
   alt2Str = 'series2D';   
   alt3Str = 'spectrum2D';
   alt4Str = 'image';   
   alt5Str = 'spectrum1D';
   alt6tr = 'volumeEchos';   
   alt7Str = 'series2DEchos';   
   alt8Str = 'series3D';   
   alt9Str = 'spectrum3D';
   
elseif strcmp(typeStr,'volume')
   alt1Str = 'imageEchos';   
   alt2Str = 'series2D';   
   alt3Str = 'spectrum2D';
   alt4Str = 'image';   
   alt5Str = 'spectrum1D';
   alt6Str = 'volumeEchos';   
   alt7Str = 'series2DEchos';   
   alt8Str = 'series3D';   
   alt9Str = 'spectrum3D';
   
elseif strcmp(typeStr,'volumeEchos')
   alt1Str = 'series2DEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';
   alt4Str = 'volume';   
   alt5Str = 'imageEchos';   
   alt6Str = 'series2D';   
   alt7Str = 'spectrum2D';
   alt8Str = 'series3DEchos';
   
elseif strcmp(typeStr,'series2D')
   alt1Str = 'imageEchos';   
   alt2Str = 'volume';   
   alt3Str = 'spectrum2D';
   alt4Str = 'image';   
   alt5Str = 'spectrum1D';
   alt6Str = 'volumeEchos';   
   alt7Str = 'series2DEchos';   
   alt8Str = 'series3D';   
   alt9Str = 'spectrum3D';
   
elseif strcmp(typeStr,'series2DEchos')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';
   alt4Str = 'volume';   
   alt5Str = 'imageEchos';   
   alt6Str = 'series2D';   
   alt7Str = 'spectrum2D';
   alt8Str = 'series3DEchos';
      
elseif strcmp(typeStr,'series3D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series3D';   
   alt3Str = 'spectrum3D';
   alt4Str = 'volume';   
   alt5Str = 'imageEchos';   
   alt6Str = 'series2D';   
   alt7Str = 'spectrum2D';
   alt8Str = 'series3DEchos';
   
elseif strcmp(typeStr,'series3DEchos')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';   
   alt4Str = 'spectrum3D';
   
elseif strcmp(typeStr,'spectrum')
   alt1Str = 'image';   
   alt2Str = 'spectrum1D';
   
elseif strcmp(typeStr,'spectrum1D')
   alt1Str = 'image'; 
   alt2Str = 'spectrum';
   alt3Str = 'imageEchos';   
   alt4Str = 'volume';
   alt5Str = 'series2D';   
   alt6Str = 'spectrum2D';
   
elseif strcmp(typeStr,'spectrum2D')
   alt1Str = 'volume';   
   alt2Str = 'imageEchos';   
   alt3Str = 'series2D';
   alt4Str = 'image';   
   alt5Str = 'spectrum1D';
   alt6Str = 'volumeEchos';   
   alt7Str = 'series2DEchos';   
   alt8Str = 'series3D';   
   alt9Str = 'spectrum3D';
   
elseif strcmp(typeStr,'spectrum3D')
   alt1Str = 'volumeEchos';   
   alt2Str = 'series2DEchos';   
   alt3Str = 'series3D';
   alt4Str = 'volume';   
   alt5Str = 'imageEchos';   
   alt6Str = 'series2D';   
   alt7Str = 'spectrum2D';
   alt8Str = 'series3DEchos';
   
end

if ~isempty(alt1Str) num = num+1; end
if ~isempty(alt2Str) num = num+1; end
if ~isempty(alt3Str) num = num+1; end
if ~isempty(alt4Str) num = num+1; end
if ~isempty(alt5Str) num = num+1; end
if ~isempty(alt6Str) num = num+1; end
if ~isempty(alt7Str) num = num+1; end
if ~isempty(alt8Str) num = num+1; end
if ~isempty(alt9Str) num = num+1; end

if num==1
   alternativesStrMx = {alt1Str};
elseif num==2
   alternativesStrMx = {alt1Str;alt2Str};
elseif num==3
   alternativesStrMx = {alt1Str;alt2Str;alt3Str};
elseif num==4
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str};
elseif num==5
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str;alt5Str};
elseif num==6
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str;alt5Str;alt6Str};
elseif num==7
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str;alt5Str;alt6Str;alt7Str};
elseif num==8
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str;alt5Str;alt6Str;alt7Str;alt8Str};
elseif num==9
   alternativesStrMx = {alt1Str;alt2Str;alt3Str;alt4Str;alt5Str;alt6Str;alt7Str;alt8Str;alt9Str};
end










% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
