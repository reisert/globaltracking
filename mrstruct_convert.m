%MRSTRUCT_CONVERT convert mrStruct from one to another type.
%		[mrStruct,errorFlag] = mrstruct_convert(mrStruct,targetTypeStr,selectDim,selectFrame)
%
%		The type of a mrStruct can be modified when the dimensionality 
%		stays the same or changes by 1 ('image'->'volume', 'series3D'->'imageEchos',...).
%		
%		'mrStruct': MR-Structure to be converted.
%		'targetTypeStr': data type in which mrStruct shall be converted.
%		mrStruct.dataAy will be resized if it is not empty.
%		In case of downcast one dimension hast to be sqeezed if mrStruct.dataAy is not empty.
%		'selectDim': String indicating the dimension to be squeezed (like 'size_t'). Or, 
%		Integer indicating the dimension to be squeezed (2 for mrStruct.dim2...).
%		'selectFrame': value of the dimension to be squeezed
%						(3 when converting 'volume' to 'image' and 'selectDim'='size_z' and the third
%						slice of mrStruct.dataAy is requested).
%		Function an be called with 2,3 or 4 arguments. As default, selectDim uses 
%		the last dimension and selectFrame=1.
%
%		Examples:
%		[mrStruct,errorFlag] = mrstruct_convert(mrStruct,'image','size_z',3)
%
%		Harald Fischer
%		1/00
%
%		PC
% ----------------------- development -----------------------------------
%
% Authors:
%   Michael von Mengershausen (MVM)
%
% Projects:
%   MVM: renaming of matlab_new 'isvector' --> 'mn_isvector' (060110)

function [mrStruct,errorFlag] = mrstruct_convert(mrStruct,targetTypeStr,selectDim,selectFrame)



%%%%% init and error check
errorFlag = 1;
if nargin==2 | nargin==3 | nargin==4
   if ~isstruct(mrStruct) | ~ischar(targetTypeStr)
      warning('input argument have wrong type');
      return;
   end
   if nargin==2 | nargin==3
      selectFrame=1;
   end
else 
   warning('insufficient input arguments');
end
%%%%% End of: init and error check



%%%%% check for valid struct
isValid = mrstruct_istype(mrStruct);
if ~isValid
   warning('not a valid mrStruct');
   return;
end
%%%%% End of: check for valid struct



%%%%% which cast?
sourceTypeStr = mrstruct_query(mrStruct,'dataType');
sourceTypeDim = mrstruct_type_ndims(sourceTypeStr);
targetTypeDim = mrstruct_type_ndims(targetTypeStr);
if nargin==2
   selectDim = sourceTypeDim;
end
if ischar(selectDim)
   selectDim = mrstruct_query(mrStruct,selectDim);
end
if sourceTypeDim==0 | targetTypeDim==0 | selectDim==0
   warning('warning: source or target type not recognized');
   return;
end

if targetTypeDim==sourceTypeDim
   castFlag=0;
elseif targetTypeDim==sourceTypeDim+1
   castFlag=1;
elseif targetTypeDim==sourceTypeDim-1
   castFlag=2;
else
   warning('warning: cannot convert data when squeezing/expanding more than one dimension');
   return;
end

if selectDim>sourceTypeDim+1 | selectDim<1
   warning('new dimension for cast is out of range');
   return;
end
%%%%% which cast?



%%%%% get defaults for insert/remove dimension
if castFlag==1     % upcast
   if nargin==2
      selectDim = mrstruct_upcast_dimension_def(sourceTypeStr,targetTypeStr);
   end
elseif castFlag==2 % downcast
   if nargin==2;
      selectDim = mrstruct_downcast_dimension_def(sourceTypeStr,targetTypeStr);
   end   
end
%%%%% End of: get defaults for insert/remove dimension



%%%%% main
if castFlag==0     % no cast of dimension
   mrStruct = local_convert_alternative(mrStruct,sourceTypeStr,targetTypeStr);
   
elseif castFlag==1 % upcast
   mrStruct = local_convert_upcast(mrStruct,sourceTypeStr,targetTypeStr,selectDim);   
   
elseif castFlag==2 % downcast
   mrStruct = local_convert_downcast(mrStruct,sourceTypeStr,targetTypeStr,selectDim,selectFrame);
   
end
errorFlag = 0;
%%%%% End of: main




%%%%%%%%%%%%%%%%%%%%% local functions %%%%%%%%%%%%%%%%%%%%%%%%




function [mrStruct] = local_convert_alternative(mrStruct,sourceTypeStr,targetTypeStr)

tmpMrStruct   = mrstruct_init(targetTypeStr);
mrStruct.dim1 = tmpMrStruct.dim1;
mrStruct.dim2 = tmpMrStruct.dim2;
mrStruct.dim3 = tmpMrStruct.dim3;
mrStruct.dim4 = tmpMrStruct.dim4;
mrStruct.dim5 = tmpMrStruct.dim5;
mrStruct.dim6 = tmpMrStruct.dim6;
mrStruct.dim7 = tmpMrStruct.dim7;
mrStruct.dim8 = tmpMrStruct.dim8;
mrStruct.dim9 = tmpMrStruct.dim9;




function [mrStruct] = local_convert_upcast(mrStruct,sourceTypeStr,targetTypeStr,insertDim)

tmpMrStruct = mrstruct_init(targetTypeStr);

if ~isempty(mrStruct.dataAy)
   
   %%% is a vector
   if mn_isvector(mrStruct.dataAy)
      if insertDim==1
         mrStruct.dataAy = shiftdim(mrStruct.dataAy,1); % Move used dimension to the end by one
      else
         % Matlab handles vectors as a Matrix. Therefore nothing has to be done here.
      end
   %%% End of: is a vector
      
      
   %%% is larger than a vector   
   else
      sizeVc    = size(mrStruct.dataAy);
      newSizeVc = zeros(length(sizeVc)+1,1);
      index     = 1;
      for newIndex=1:length(sizeVc)+1
         if newIndex==insertDim
            newSizeVc(newIndex) = 1;
         else
            newSizeVc(newIndex) = sizeVc(index);
            index = index+1;
         end
      end
      mrStruct.dataAy = reshape(mrStruct.dataAy,newSizeVc');
   end
   %%% End of: is larger than a vector   

end
mrStruct.dim1 = tmpMrStruct.dim1;
mrStruct.dim2 = tmpMrStruct.dim2;
mrStruct.dim3 = tmpMrStruct.dim3;
mrStruct.dim4 = tmpMrStruct.dim4;
mrStruct.dim5 = tmpMrStruct.dim5;
mrStruct.dim6 = tmpMrStruct.dim6;
mrStruct.dim7 = tmpMrStruct.dim7;
mrStruct.dim8 = tmpMrStruct.dim8;
mrStruct.dim9 = tmpMrStruct.dim9;




function [mrStruct] = local_convert_downcast(mrStruct,sourceTypeStr,targetTypeStr,selectDim,selectFrame)

tmpMrStruct   = mrstruct_init(targetTypeStr);

if ~isempty(mrStruct.dataAy)
   if mn_isvector(mrStruct.dataAy)
      warning('Can not downcast any more. Data already of vector type');   
   else
      sizeVc   = size(mrStruct.dataAy);
      noOfDims = mrstruct_query(mrStruct,'dimensions');
      
      
      %%% out of range error
      if selectDim>noOfDims | selectFrame>size(mrStruct.dataAy,selectDim)
         warning('Can not squeeze dimension');
      %%% End of: out of range error
      
      
      %%% there are singleton dimensions at the end of mrStruct.dataAy
      elseif noOfDims>length(sizeVc) & selectDim>length(sizeVc)
         %okay
      %%% End of: there are singleton dimensions at the end of mrStruct.dataAy
      
      
      %%% squeeze a dimension in between   
      else
         tmpStr='';
         for index=1:length(sizeVc)
            if  selectDim==index
               tmpStr = sprintf('%s%d,',tmpStr,selectFrame);
            else
               tmpStr = sprintf('%s1:%d,',tmpStr,sizeVc(index));
            end
         end
         tmpStr = tmpStr(1:length(tmpStr)-1);
         cmdStr = sprintf('mrStruct.dataAy = mrStruct.dataAy(%s);',tmpStr);
         eval(cmdStr);
         reshapeVc = sizeVc;
         reshapeVc(selectDim) = [];
         if length(sizeVc)==2 % result is a vector
            mrStruct.dataAy = mrStruct.dataAy(:);
         else
            mrStruct.dataAy = reshape(mrStruct.dataAy,reshapeVc);
         end
      end
      %%% End of: squeeze a dimension in between
      
   end
end

mrStruct.dim1 = tmpMrStruct.dim1;
mrStruct.dim2 = tmpMrStruct.dim2;
mrStruct.dim3 = tmpMrStruct.dim3;
mrStruct.dim4 = tmpMrStruct.dim4;
mrStruct.dim5 = tmpMrStruct.dim5;
mrStruct.dim6 = tmpMrStruct.dim6;
mrStruct.dim7 = tmpMrStruct.dim7;
mrStruct.dim8 = tmpMrStruct.dim8;
mrStruct.dim9 = tmpMrStruct.dim9;



% Copyright (c) May 15th, 2001 by University of Freiburg, Dept. of Radiology, Section of Medical Physics
