function [mrs nii_orig] = DPS_nifti_to_hardi(niiName,bvecname,bvalname,bmatname)

nii_orig = load_untouch_nii(niiName);
nii = nii_orig;

mrs = DPS_nifti_to_mrs(nii);

T=1;

if nargin == 4 & not(isempty(bmatname)),
        bmats = importdata(bmatname{1});
        bmats = permute(reshape(bmats,[size(bmats,1) 3 3]),[2 3 1]);
        bval = squeeze(bmats(1,1,:)+bmats(2,2,:)+bmats(3,3,:));
        bvec = [];
        tensor = bmats;
              T = [0 -1 0; ...
                   -1  0 0; ...
                   0  0 1];
    %           T = [1 0 0; ...
    %                0 1 0; ...
    %                0  0 1];
        for k =1:size(tensor,3),
            tensor(:,:,k) = T'*tensor(:,:,k)*T;
        end;
else

        if exist('bvecname') & isstruct(bvecname)
            bval = strsplit(bvecname.bval,' ');
            bval = cellfun(@str2num,bval(1:end-1));

            bvec_ = strsplit(bvecname.bvec,'\n');
            for k = 1:3
                a = strsplit(bvec_{k},' ');
                bvec(k,:) = cellfun(@str2num,a(1:end-1));
            end
            if isfield(bvecname,'bmat')
                bmat_ = strsplit(bvecname.bmat,'\n');
                for k = 1:length(bmat_)-1
                    a = strsplit(bmat_{k},' ');
                    bmat(k,:) = cellfun(@str2num,a(1:end-1));
                end
            end
        else

        if not(exist('bvecname')) || isempty(bvecname)
            niinogz = strrep(niiName,'.gz','');
            bvecname = [niinogz(1:end-3), 'bvec'];
        end

        if not(exist('bvalname'))|| isempty(bvalname)
            niinogz = strrep(niiName,'.gz','');        
            bvalname = [niinogz(1:end-3), 'bval'];
        end

        bvec = importdata(bvecname);
        bval = importdata(bvalname);    

    end
    
    if size(bvec,1) == length(bval);
            bvec = bvec';
    end;

    bval = bval(:)';

    for k = 1:size(bval,2),
        gdir = bvec(:,k);
        gdir = gdir / (eps+norm(gdir));
        tensor(:,:,k) = gdir*gdir' *bval(k);
    end;
    
    bvalthres = 150; %% all img below this b-value are designated as b0
    
    bdir = bvec.* repmat(bval>bvalthres,[3 1]);
    
    

end    
    
mrs.user.bfactor = bval;
mrs.user.bDir = bdir;
mrs.user.bTensor = tensor;

if isstruct(bvecname) && isfield(bvecname,'intended_bvecname')
    bvecname = bvecname.intended_bvecname;
end

if not(isempty(strfind(bvecname,'.bvec'))) && exist(strrep(bvecname,'.bvec','.bexclude'),'file')
    fid = fopen(strrep(bvecname,'.bvec','.bexclude'),'r');
    data = native2unicode(fread(fid,inf,'*uint8')','UTF-8');
    fclose(fid);
    bexclude = [];
    lines = strsplit(data,[10]);
    for k = 1:length(lines),
        toks = strsplit(lines{k},' ');
        if isempty(toks)
            continue;
        end;
        if not(strcmp(toks{1}(1),'#'))
            bexclude = [bexclude cellfun(@str2num,toks)];
        end
    end;
    
    binclude = setdiff(1:length(mrs.user.bfactor),bexclude+1);
    mrs.dataAy = mrs.dataAy(:,:,:,binclude);
    mrs.user.bfactor = mrs.user.bfactor(binclude);
    mrs.user.bDir = mrs.user.bDir(:,binclude);
    mrs.user.bTensor = mrs.user.bTensor(:,:,binclude);       
end;

nii = rmfield(nii,'img');
mrs.user.nii = nii;

end

